import {createApp} from 'vue'
import {createPinia} from 'pinia'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { useUserStore } from '@/stores/user'

const app = createApp(App)
app.use(createPinia())
app.use(router)
axios.defaults.headers.common['Authorization'] = 'Bearer ' + useUserStore().$state.token
app.use(VueAxios, axios)
app.provide('axios', app.config.globalProperties.axios)  // provide 'axios'
app.mount('#app')
