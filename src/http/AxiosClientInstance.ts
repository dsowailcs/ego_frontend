import axios, {AxiosInstance} from 'axios';

export const axiosClientInstance: AxiosInstance = axios.create({
    baseURL: `https://jsonplaceholder.typicode.com/`,
    /*    headers: {
            Authorization: 'Bearer {token}'
        }*/
})