import {PostRepositoryInterface} from "@/repository/PostRepositoryInterface";
import {AxiosInstance} from "axios";
import {axiosClientInstance} from "@/http/AxiosClientInstance";

export interface PostsRepositoryDataInterface {
    userId: number,
    id: number,
    title: string,
    body: string
}

interface ConstructorParams {
    axiosClientInstance: AxiosInstance;
}

class PostsRepository implements PostRepositoryInterface {
    private _axiosClient: any;

    public constructor({axiosClientInstance}: ConstructorParams) {
        this._axiosClient = axiosClientInstance;
    }

    public getPosts(): Promise<PostsRepositoryDataInterface[]> {
        return this._axiosClient
            .get('posts')
            .then((res: any) => res.data)
            .catch((err: any) => {
                console.warn(err)
                return null
            })
    }
}
export const postsRepository = new PostsRepository({axiosClientInstance})


